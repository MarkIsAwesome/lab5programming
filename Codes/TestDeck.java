import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.Test;

public class TestDeck {
    @Test
    public void test_addToDeck() {
        Deck deck = new Deck();

        ArrayList<UnoCard> unoDeck = deck.getDeck();
        int deckSize = unoDeck.size() +1;

        deck.addToDeck(new ReverseCards("Blue", "reverse"));
        int actual = unoDeck.size();


        assertEquals(deckSize, actual);
    }

    @Test
    public void numWild() {
        Deck deck = new Deck();

        ArrayList<UnoCard> cards = deck.getDeck();
        int countWild = 0;
        for (UnoCard wild : cards) {
            if (wild instanceof WildCards) {
                countWild++;
            }
        }

        assertEquals(countWild, 8);

    }

    @Test
    public void numNumberedCards() {
        Deck deck = new Deck();

        ArrayList<UnoCard> uno = deck.getDeck();
        int count = 0;

        for (UnoCard num : uno) {
            if (num instanceof NumberedCards) {
                count++;
            }
        }

        assertEquals(count, 80);
    }

    @Test
    public void numSkipCards() {
        Deck deck = new Deck();

        ArrayList<UnoCard> uno = deck.getDeck();
        int count = 0;

        for (UnoCard skip : uno) {
            if (skip instanceof SkipCards) {
                count++;
            }
        }

        assertEquals(count, 8);
    }

    @Test
    public void numReverseCards() {
        Deck deck = new Deck();

        ArrayList<UnoCard> uno = deck.getDeck();
        int count = 0;

        for (UnoCard reverse : uno) {
            if (reverse instanceof ReverseCards) {
                count++;
            }
        }

        assertEquals(count, 8);
    }

    @Test
    public void numPlusTwoCards() {
        Deck deck = new Deck();

        ArrayList<UnoCard> uno = deck.getDeck();
        int count = 0;

        for (UnoCard plusTwo : uno) {
            if (plusTwo instanceof PickUpTwoCards) {
                count++;
            }
        }

        assertEquals(count, 8);
    }
}
