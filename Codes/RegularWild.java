public class RegularWild extends WildCards {
    private String color;
    
    public RegularWild() {
        this.color = getColor();
    }

    public String getColor() {
        return this.color;
    }

    public boolean canPlay(UnoCard card) {
        return true;
    }
}
