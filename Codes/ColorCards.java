

public abstract class ColorCards implements UnoCard {
    protected String color;
    
    public ColorCards(String color) {
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }
}
