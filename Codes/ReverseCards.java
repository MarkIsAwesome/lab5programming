public class ReverseCards extends ColorCards{
    private String reverse;

    public ReverseCards(String color, String reverse) {
        super(color);
        this.color = color;
        this.reverse = reverse;
    }

    public String getReverse() {
        return this.reverse;
    }

    public boolean canPlay(UnoCard card) {
        if (card instanceof ReverseCards) {
            if(this.color.equals(((ReverseCards) card).getColor()) || this.reverse.equals(((ReverseCards) card).getReverse())) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
}
