public class WildPlusFour extends WildCards {
    private String color;
    private int plusFour;

    public WildPlusFour() {
        this.color = getColor();
        this.plusFour = 2;
    }

    public String getColor() {
        return this.color;
    }
    public int getPlusFour() {
        return this.plusFour;
    }


    public boolean canPlay(UnoCard card) {
        return true;
    }
}
