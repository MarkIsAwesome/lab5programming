public class SkipCards extends ColorCards {
    private String skip;
    
    public SkipCards(String color, String skip) {
        super(color);
        this.color = color;
        this.skip = skip;
    }
    
    public String getSkip() {
        return this.skip;
    }
        
    public boolean canPlay(UnoCard card) {
        if (card instanceof SkipCards) {
            if (this.color.equals(((SkipCards) card).getColor()) || this.skip.equals(((SkipCards) card).getSkip())) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
}