import java.util.ArrayList;

public class Deck {
    private ArrayList<UnoCard> deck = new ArrayList<UnoCard>();

    public Deck() {

        for (int i = 0; i < 4; i++) {
            this.deck.add(new RegularWild());
        }

        for (int i = 0; i < 4; i++) {
            this.deck.add(new WildPlusFour());
        }

        for (int i = 0; i <=9; i++) {
            for (int j = 1; j <=2; j++) {
                this.deck.add(new NumberedCards("Blue", i));
                this.deck.add(new NumberedCards("Red", i));
                this.deck.add(new NumberedCards("Yellow", i));
                this.deck.add(new NumberedCards("Green", i));
            }
        }

        for (int i = 1; i <=2; i++) {
            this.deck.add(new SkipCards("Blue", "skip"));
            this.deck.add(new SkipCards("Red", "skip"));
            this.deck.add(new SkipCards("Yellow", "skip"));
            this.deck.add(new SkipCards("Green", "skip"));
        }

        for (int i = 1; i <=2; i++) {
            this.deck.add(new ReverseCards("Blue", "reverse"));
            this.deck.add(new ReverseCards("Red", "reverse"));
            this.deck.add(new ReverseCards("Yellow", "reverse"));
            this.deck.add(new ReverseCards("Green", "reverse")); 
        }

        for (int i=1 ; i<=2;i++) {
            this.deck.add(new PickUpTwoCards("Blue"));
            this.deck.add(new PickUpTwoCards("Red"));
            this.deck.add(new PickUpTwoCards("Yellow"));
            this.deck.add(new PickUpTwoCards("Green"));
        }
    }

    public void addToDeck(UnoCard card) {
        this.deck.add(card);
    }

    public UnoCard draw() {
        UnoCard drawCard = this.deck.get(0);
        this.deck.remove(drawCard);
        return drawCard;
    }

    public ArrayList<UnoCard> getDeck() {
        return this.deck;
    }
}