public class PickUpTwoCards extends ColorCards {
    private int plusTwo;

    public PickUpTwoCards(String color) {
        super(color);
        this.color = color;
        this.plusTwo = 2;
    }

    public int getPlusTwo() {
        return this.plusTwo;
    }

    public boolean canPlay(UnoCard card) {
        if (card instanceof PickUpTwoCards) {
            if(this.color.equals(((PickUpTwoCards) card).getColor()) || this.plusTwo == ((PickUpTwoCards)card).getPlusTwo()) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
}
