public class NumberedCards extends ColorCards {
    private int numCards;


    public NumberedCards( String color, int numCards) {
        super(color);
        this.numCards = numCards;
        this.color = color;
    }

    public int getNumCard() {
        return this.numCards;
    }


    public boolean canPlay(UnoCard card) {
        if (card instanceof NumberedCards) {
            if (this.color.equals(((NumberedCards) card).getColor()) || this.numCards == ((NumberedCards) card).getNumCard()) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

}
