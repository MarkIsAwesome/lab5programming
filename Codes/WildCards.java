public abstract class WildCards implements UnoCard {
    protected String color;

    public WildCards() {
        this.color = "black";
    }

    public String getColor() {
        return this.color;
    }
}
